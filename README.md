# Bikemap
This project contains:
* bike route database model
* bike route REST API CRUD on `routes/api/v1/routes/`; it uses GeoJSON format
* simple HTML route detail view on `routes/routes/<pk>/`
* simple admin panel

## How to run the project
Copy `.env.example` to `.env` and run
```
docker-compose up
```
Open browser on `localhost:8000`

## How to create admin user
```
docker-compose run django python src/manage.py createsuperuser
```

## How to run API tests
```
docker-compose run django sh -c "/wait && cd src && pytest"
```