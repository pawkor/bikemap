from django.urls import include, path
from rest_framework.routers import DefaultRouter

from routes.api import viewsets

app_name = "api"

router = DefaultRouter()
router.register(r"routes", viewsets.RouteViewSet)

urlpatterns = [
    path("v1/", include(router.urls)),
]
