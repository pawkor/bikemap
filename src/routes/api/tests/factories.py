from django.contrib.gis.geos import LineString, MultiLineString
from factory.django import DjangoModelFactory

from routes.models import Route


class RouteFactory(DjangoModelFactory):
    class Meta:
        model = Route

    name = "Test"
    polyline = MultiLineString(LineString((1, 1), (2, 2)))
