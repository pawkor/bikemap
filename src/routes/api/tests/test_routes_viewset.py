import pytest
from django.urls import reverse
from rest_framework import status

from routes.api.tests.factories import RouteFactory
from routes.models import Route


@pytest.mark.django_db
def test_delete_single_object(client):
    route = RouteFactory()
    url = reverse("routes:api:route-detail", args=(route.pk,))
    response = client.delete(url)
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert not Route.objects.all().exists()


@pytest.mark.django_db
def test_create_invalid_polyline(client):
    url = reverse("routes:api:route-list")
    response = client.post(
        url,
        data={"name": "New name", "polyline": "invalid value"},
        content_type="application/json",
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert not Route.objects.all().exists()


@pytest.mark.django_db
def test_create_missing_polyline_dont_create_object(client):
    url = reverse("routes:api:route-list")
    response = client.post(
        url,
        data={"name": "New name"},
        content_type="application/json",
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert not Route.objects.all().exists()


@pytest.mark.django_db
def test_create_missing_name_dont_create_object(client):
    url = reverse("routes:api:route-list")
    response = client.post(
        url,
        data={
            "polyline": {
                "type": "MultiLineString",
                "coordinates": [
                    [
                        [10, 11],
                        [12, 13],
                    ]
                ],
            }
        },
        content_type="application/json",
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert not Route.objects.all().exists()


@pytest.mark.django_db
def test_create(client):
    url = reverse("routes:api:route-list")
    response = client.post(
        url,
        data={
            "name": "New name",
            "polyline": {
                "type": "MultiLineString",
                "coordinates": [
                    [
                        [10, 11],
                        [12, 13],
                    ]
                ],
            },
        },
        content_type="application/json",
    )
    assert response.status_code == status.HTTP_201_CREATED
    assert Route.objects.all().count() == 1
    route = Route.objects.first()
    assert response.json() == {
        "id": route.id,
        "type": "Feature",
        "geometry": {
            "type": "MultiLineString",
            "coordinates": [
                [
                    [10, 11],
                    [12, 13],
                ]
            ],
        },
        "properties": {"name": "New name"},
    }
    assert route.name == "New name"
    assert len(route.polyline) == 1
    assert route.polyline[0].x == [10, 12]
    assert route.polyline[0].y == [11, 13]


@pytest.mark.django_db
def test_replace_single_object(client):
    route = RouteFactory()
    url = reverse("routes:api:route-detail", args=(route.pk,))
    response = client.put(
        url,
        data={
            "name": "New name",
            "polyline": {
                "type": "MultiLineString",
                "coordinates": [
                    [
                        [10, 11],
                        [12, 13],
                    ]
                ],
            },
        },
        content_type="application/json",
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {
        "id": route.id,
        "type": "Feature",
        "geometry": {
            "type": "MultiLineString",
            "coordinates": [
                [
                    [10, 11],
                    [12, 13],
                ]
            ],
        },
        "properties": {"name": "New name"},
    }
    route.refresh_from_db()
    assert route.name == "New name"
    assert len(route.polyline) == 1
    assert route.polyline[0].x == [10, 12]
    assert route.polyline[0].y == [11, 13]


@pytest.mark.django_db
def test_update_single_object(client):
    route = RouteFactory()
    url = reverse("routes:api:route-detail", args=(route.pk,))
    response = client.patch(
        url,
        data={
            "name": "New name",
            "polyline": {
                "type": "MultiLineString",
                "coordinates": [
                    [
                        [10, 11],
                        [12, 13],
                    ]
                ],
            },
        },
        content_type="application/json",
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {
        "id": route.id,
        "type": "Feature",
        "geometry": {
            "type": "MultiLineString",
            "coordinates": [
                [
                    [10, 11],
                    [12, 13],
                ]
            ],
        },
        "properties": {"name": "New name"},
    }
    route.refresh_from_db()
    assert route.name == "New name"
    assert len(route.polyline) == 1
    assert route.polyline[0].x == [10, 12]
    assert route.polyline[0].y == [11, 13]


@pytest.mark.django_db
def test_retrieve_single_object(client):
    route = RouteFactory()
    url = reverse("routes:api:route-detail", args=(route.pk,))
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {
        "id": route.id,
        "type": "Feature",
        "geometry": {
            "type": "MultiLineString",
            "coordinates": [
                [
                    [1, 1],
                    [2, 2],
                ]
            ],
        },
        "properties": {"name": "Test"},
    }


@pytest.mark.django_db
def test_retrieve_returns_404_for_not_existing(client):
    url = reverse("routes:api:route-detail", args=(123,))
    response = client.get(url)
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_retrieve_all_object(client):
    route = RouteFactory()
    url = reverse("routes:api:route-list")
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {
        "count": 1,
        "next": None,
        "previous": None,
        "results": {
            "type": "FeatureCollection",
            "features": [
                {
                    "id": route.id,
                    "type": "Feature",
                    "geometry": {
                        "type": "MultiLineString",
                        "coordinates": [
                            [
                                [1, 1],
                                [2, 2],
                            ]
                        ],
                    },
                    "properties": {"name": "Test"},
                }
            ],
        },
    }


@pytest.mark.django_db
def test_retrieve_all_object_pagination(client):
    RouteFactory.create_batch(200)
    url = reverse("routes:api:route-list")
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    json_response = response.json()
    assert json_response["count"] == 200
    assert len(json_response["results"]["features"]) == 100
