from rest_framework import viewsets

from routes.api.serializers import RouteSerializer
from routes.models import Route


class RouteViewSet(viewsets.ModelViewSet):
    queryset = Route.objects.all()
    serializer_class = RouteSerializer
