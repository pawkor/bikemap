from rest_framework_gis.pagination import GeoJsonPagination
from rest_framework_gis.serializers import GeoFeatureModelSerializer

from routes.models import Route


class RouteSerializer(GeoFeatureModelSerializer):
    pagination_class = GeoJsonPagination

    class Meta:
        model = Route
        geo_field = "polyline"
        fields = ("id", "name", "polyline")
