from django.contrib.gis.db import models


class Route(models.Model):
    name = models.CharField(max_length=256)
    polyline = models.MultiLineStringField()

    def __str__(self):
        return self.name
