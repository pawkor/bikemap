from django.urls import include, path

from routes import views

app_name = "routes"

urlpatterns = [
    path("api/", include("routes.api.urls", namespace="api")),
    path("routes/<int:pk>/", views.RouteDetailView.as_view(), name="detail"),
]
