from django.views.generic import DetailView

from routes.models import Route


class RouteDetailView(DetailView):
    model = Route
